
const events = {
    "Treasure Hunter Hunting": {
        description: "A curious pack of Dromaeosaur is collecting shiny baubles in the area. Tracking them to their nest reveals a significant find. Luring them away to pilfer safely could be quite lucrative.\n\nBeasts (Soul) - Carnivore food supplies may also be used for this check\n\nPass: The predators are safely lured away, leaving their treasures unattended.\n\nFail: You are lured into the nest by the clever predators. Some are still waiting in ambush while others raid your camp. Lose food supplies and make an Injury Check for the check leader.",
        check: {
            attribute: 'soul',
            type: 'Beasts',
            fail: {
                injuryCheckLeader: true,
            }
        }
    },
    "Pterodactyl Ascent": {
        description: "An interesting find is spotted on top of a large rock face. The climb doesn't look so bad itself, but on closer inspection, there is a colony of flying dinosaurs near the base of the ascent.\n\nTravel (Body)\n\nPass: A steady and confident ascent up the rock face draws little attention from the colony, and you make it to the apex in good time.\n\nFail: A difficult section on the climb has you retracing your ascent path a few times. The fatigue and distress draw the attention of the colony, who begin to circle above you. You do your best to descend quickly before being forced to jump to avoid becoming a snack. Make an Injury check for all characters.",
        check: {
            attribute: 'body',
            type: 'Travel',
            fail: {
                injuryCheckAll: true,
            }
        }
    },
    "Primal Territory": {
        description: "A vast area is clearly marked by an outline of shattered trees, trampled vegetation, and massive footprints. Something primal has marked its territory here. To proceed through this region will require extreme caution.\n\nStealth (Mind)\n\nPass: You travel slowly and against the wind. Over many weeks of sleepless nights and close encounters, you reach the other side of the territory.\n\nFail: You never see it, but you can feel the crashing footsteps rushing towards camp.\n\n- Travel (Body)\n\nPass: Without time to pack camp, you flee into the darkness of the night with what you can carry. Lose all lifestyle supplies.\n\nFail: The terrain has been harsh enough moving slowly. You know you are doomed if you pause for even a moment before fleeing. Reduce supplies down to 1 per character, lifestyle supplies may not be chosen."
    },
    "Sleeping Giants": {
        description: "A family of giant sauropods blocks the entrance to a narrow pass. They are not hostile but are easily spooked and could do great damage to the expedition just by stepping in the wrong place. You could try and sneak through while they are asleep.\n\nStealth (Body) - Difficulty + 2\n\nPass: You successfully navigate around the giants as they sleep.\n\nFail: One of the sauropods awakens as the expedition is passing by. Not sure what they think of the group, they briefly pause to examine you closer.\n\n- Beasts (Soul)\n\nPass: You quietly and calmly talk the giant back to sleep. You may repeat the event from the beginning with a +2 difficulty or abandon the reward safely and stay in this location another month until the family moves on.\n\nFail: You back away from the giant slowly, but unable to sniff you clearly, they begin calling to the rest of the family in alarm, and you are forced to flee from the panicked footfalls and swinging tails. Make an Injury Check for all characters."
    }
};

const modifiers = {
    "Game Trail Buffet": {
        description: "A prominent game trail is the only viable path to your destination. You have ample food supplies available but have to carefully avoid becoming a snack yourself.\n\nAll non-eccentric food supplies are refunded for this month.\n\nTreat the location Risk as if it were double for this month."
    },
    "Thundering Herd": {
        description: "A gentle herd of Ankylosaurs is migrating through the area. With a little nudge, these majestic beasts could be herded into an advantageous position.\n\n+2 limit on checks."
    },
    "Rough Housing Hadrosaur": {
        description: "A friendly Hadrosaur begins following the expedition and mimicking your activities, which is actually rather helpful for some tasks. However, they are quite hungry and clumsy with their bulk.\n\nOptional: Spend one vegetarian food supply and make a Beasts (Body) check. If passed, add the success count over the difficulty to all other checks this event."
    },
    "Stegosaurus Serenity": {
        description: "Your route takes you through a hidden glade guarded by a clan of stegosaurus. Not sure what to think of your expedition, they mostly leave you alone.\n\n+1 Vegetarian supplies\n+1 Survivalist supplies"
    }
};

const rewards = {
    "An Abandoned Kill": {
        description: "A nearly untouched kill is left alone. Grab all the meat you can carry before other scavengers arrive.\n\n(Carnivore Supplies)",
        supplies: ['Carnivore Diet']
    },
    "A Small Egg": {
        description: "A small egg is left abandoned in a nest. If properly cared for and trained, it could grow up to be a lifetime companion.\n\nThe Dinosaur Companion is an item and must be assigned and owned immediately or be discarded.\n\nThe Dinosaur Companion begins with a -2 modifier to all limit and pool for checks led by the owner.\n\nAt the end of every month, the owner makes a Beasts (Soul) check against the current modifier. Companion bonuses cannot be added to this check. If successful, the modifier is increased by 1."
    },
    "A Large Egg": {
        description: "A very large egg sits alone in a clearing, ready to hatch at any time. Add a dinosaur character to the expedition with the following characteristics: 4d6+reward/2 body, 1d6 mind, 1d6 soul, primal background, other traits: Growing (attributes are rolled 1 die per month until they have reached their final value), Ravenous (this character consumes triple the food supplies)."
    },
    "Tar Pit": {
        description: "A small tar bog has claimed many unsuspecting victims, leaving abundant bones, tissue samples, and the tar itself to craft field supplies with or haul back to trade.\n\nChoose one:\n\n(Injury Supplies)\n\n(Combat Supplies)\n\n(1/2 * Purchasing Supplies)"
    }
};


module.exports = { events, modifiers, rewards };
