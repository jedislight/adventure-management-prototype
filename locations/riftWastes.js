const events = {
    "Thermal Shock": {
        description: "The expedition has found a location of interest, but the surrounding landscapes are chaotic weave hellscapes frozen to molten. The trek through is relatively simple other than the inability to stay in one place for long and the constant gear shifts.\n\nTravel (Soul)\n\nPass: The expedition endures the journey, never staying too hot or cold for long.\n\nFail: The expedition hits an unexpected rift too many and has to make a judgment call between pushing through anyway and turning back.\n\nOne of: Turn back\n\nOr: Push on, keep the reward but Injury Check (All Characters)."
    },
    "Flood Plains": {
        description: "The expedition spots something interesting in a lifeless sea of brine. After some observation, the waters recede and return on a seemingly unpredictable pattern. With luck and haste, the expedition might be able to get to the site and back before the next tide.\n\nTravel (Body)\n\nPass: The expedition races out into the flood plain and narrowly avoids being swept away.\n\nFail: The tide comes in sooner than you had hoped and sweeps the expedition back to shore, violently throwing expedition members against the ground in brine surf along the way.\n\nInjury Check (All Characters).",
        check: {
            attribute: 'body',
            type: 'Travel',
            fail: {
                injuryCheckAll: true,
            }
        }
    },
    "Explosive Rift": {
        description: "The expedition finds an unfortunate pairing of rift boundaries mixing some incompatible soils. The boundary is constantly burning and occasionally exploding in an ongoing chain reaction. Between the fumes and the fires, it is unlikely the expedition will be able to make it across.\n\nTechnology (Mind)\n\nPass: The expedition does some chemical analysis and finds a local compound that can shift the reaction slightly into creating a strong acid. It still isn't safe, but it's at least contained in pools.\n\nFail: The expedition can't stop the reaction at scale, but they can slow it down on the members and gear of the expedition enough to cross, or so they thought.\n\n- One Of: Pull back and be stuck in this location for the month. Plus Injury (Check Leader)\n\n- Or: Push through Injury Check (All Characters), lose (Random Supplies), but still gain the reward."
    },
    "Monolith": {
        description: "A lone monolith is found near a site of interest. All is well. All is monolith.\n\nMysticism (Soul)\n\nPass: The monolith is clearly enchanted by some sort of evil spirit. The expedition builds a mobile tarp to avoid gazing at its lovely, wonderful... no no keep moving.\n\nFail: The monolith is a large rock. A very evil and magically charming large rock, but just a rock. Eventually, your hunger outweighs the strong desire to stay near to the monolith, but you are not sure how long has passed.\n\nLose 1 month at this location without any supply expenditure, and Injury Check (All Characters)."
    }
};

const modifiers = {
    "Barren": {
        description: "Normally expeditions supplement their own food and shelter supplies with locally sourced materials, but here, this is just nothing.\n\nPay the lifestyle and diet supplies for the expedition again this month."
    },
    "Sand Everywhere": {
        description: "The expedition must avoid unpacking their supplies to avoid contaminating their stocks.\n\nAll supplies used this month lose an additional unit at the end of the month."
    },
    "Toxic Air": {
        description: "-4 to pool/limit"
    },
    "Acid Sleet": {
        description: "Partially frozen acid rain is just as safe as it seems.\n\nInjury Check 1/2 difficulty (All characters)."
    }
};

const rewards = {
    "A Lone Desiccated Cactus": {
        description: "At some point, this cactus might have held water, but now it's a tough husk. With enough hydration, it might be edible.\n\n(Vegetarian Diet Supplies # Reward Check)",
        checkedSupplies: ["Vegetarian Diet"]
    },
    "A Hole Underneath a Boulder": {
        description: "Shelter is rare in the wastes, you take what you can get.\n\nRefund Reward Check survivalist supplies spent this month (if any)."
    },
    "Burial Shelter": {
        description: "Someone else has been through here at some point and has left behind a small memorial with a few keepsakes and some personal things. A few miles farther and there is one person camp with minimal supplies and a single skeleton.\n\nOne of: Respect the body and bury them next to what you assume is their friend. +1 morale all characters\n\nOr: Loot the camp's supplies, they don't need it anymore (Reward check random supplies, chosen individually)\n\nOr: Loot the camp and the grave. (Reward check random supplies + 1, chosen individually), -1 morale all characters."
    },
    "Toxic Waste": {
        description: "There is a lake of neon green slime. The high-energy properties are unstable in the field but would be easy to trade back at base. However, the expedition isn't equipped to handle these materials safely.\n\nUp to (Reward x2) purchasing supplies, but for each supply taken: Injury Check 1 character."
    }
};


module.exports = { events, modifiers, rewards };
