const events = {
    "Haunted Ground": {
        description: "The expedition comes across an interesting site, an easy win with no risks or complications. Shortly after though things start going wrong, minor things at first: equipment failures and the like. After too many things go wrong the expedition gets suspicious - and then the really bad things begin: folks stuck floating midair, hallucinations, terrain changing when you turn around.\n\nMysticism (Soul)\n\nPass: You are being haunted by some sort of ghost. The expedition attempts to commune with the being, but once the expedition has agreed on calling the force by a name 'ghost', the occurrences stop immediately.\n\nFail: You are being haunted by some sort of spirit, a ghost or a demon maybe? The expedition tries to backtrack and undo their interference, but the happenings just keep getting worse and worse. Eventually, everything goes black, and the expedition is back to the day before they came across the site. Everyone tries to laugh off the bad dream - but they all remember what happened.\n\n-2 Morale (All Characters)",
        check: {
            attribute: 'soul',
            type: 'Mysticism',
            fail: {
                moraleAll: -2,
            }
        }
    },
    "Possession": {
        description: "A member of the expedition starts acting strangely; they are suddenly more capable but also start disappearing for long stretches of time.\n\nSpecial: If there is only one character in the expedition, skip the rest of this and the next 1d6 months for this expedition.\n\nSocial (Mind)\n\nPass: The expedition member is clearly possessed by a wandering spirit. When confronted, the spirit confesses and explains why they needed the host. Working together, you can easily accomplish the spirit's goals, and in return, they assist you with yours before leaving.\n\nFail: The expedition member is just working hard and playing hard - what they do on their own time is their business. Besides, their productivity has not decreased overall.\n\nRandom expedition member gains Other Trait: Minor Possession - +4 limit and pool to all checks, but if any of the first 3 die rolled are a 6, the check automatically fails."
    },
    "Silence": {
        description: "A point of interest is loomed over by a large glass statue. On approach, there is a feeling of terror emanating from it.\n\nEngineering (Body) - The check leader must first pass an Untyped (Soul) Check or take -1 morale, and a new check leader is chosen.\n\nPass: The statue is terrifying, but a member of the expedition manages to summon up the will to fight instead of freeze or flee and shatters the silence.\n\nFail: ..."
    },
    "The Riddle": {
        description: "The expedition spots an interesting site along the route. When they arrive, they also find a fortune cookie with a simple riddle inside.\n\nSpecial: The reward is always gained.\n\nSocial (Mind)\n\nPass: The riddle is a paradox and has no answer; it is a trap for the weak-minded.\n\nFail: The riddle is fascinating to the expedition and begins to center in all downtime discussion, causing numerous opposing camps of thought and arguments.\n\nRandomly select (Risk check) characters and reduce their teamwork with each other by 1."
    }
};

const modifiers = {
    "Spirits": {
        description: "The area is inhabited by mischievous spirits, their intentions unknowable.\n\nDuring generation determine one of, then after choosing this event determine the other:\n\nA) Either +1 / -1\n\nB) to one of (Limit, Pool, Morale, Risk, Reward, Difficulty, one carried supply type, all carried supply types)"
    },
    "Abominable Critters": {
        description: "Your route takes you into an area with heavy wildlife, many of which are twisted nightmare versions of their former beings. Twisted or not, they are still harmless.\n\n-1 morale to all characters w/o Riftwalker background"
    },
    "Emotional Amplifier": {
        description: "For some reason, this area makes you feel more intensely.\n\nMorale moves 1 step away from 0. If at 0, step is random."
    },
    "Illusions": {
        description: "This seems so unlikely, are we sure this is real?\n\n+1d6 Reward, but on an even number total reward - there is no reward."
    }
};

const rewards = {
    "Ritual Book": {
        description: "The expedition comes across a small library of rituals, most of which are either unspeakable or incomprehensible. One, however, stands out as useful.\n\nItem: Ritual Book (Random supply when acquired) Any supply may be used in place of (supply) at a 3:1 ratio, 2:1 when converting from Mysticism Supplies."
    },
    "Joy Font": {
        description: "The expedition spots a location that makes them feel happy. There is nothing special about the location in the slightest, but the entire expedition wants to visit it.\n\n+2 morale to all characters",
        moraleAll: 2
    },
    "Survivor": {
        description: "The expedition has spotted a bunker with a single occupant barely scraping by.\n\nRoll 1d6 when acquired:\n\n1-2: The bunker was built around another point of interest, but the survivor wants nothing to do with the expedition. Draw a new reward.\n\n3-4: The survivor is grateful to be rescued. Scout a new character and add them to the expedition with Other Trait: Rescued - when returning to the base location, this character may be disbanded for (Reward x 2) purchasing supplies or remove this trait.\n\n5-6: The survivor is possessed by a peaceful spirit who ensures you their host was a very bad person. Scout a new character and add them to the expedition with background Riftwalker and Other Trait: Zen Spirit - morale is always treated as 1."
    },
    "Knowledge Totem": {
        description: "The expedition spots a small object, like a rock or empty jar of jam - it contains the secrets of the universe.\n\nItem + 1 Pool for (Roll Reward Check supply types, summing duplicates)"
    }
};

module.exports = { events, modifiers, rewards };