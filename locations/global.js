const modifiers = {
    "Heavy Rain": {
        description: "Soaked gear, terrible terrain, limited visibility.\n\n-1 Limit and Pool size."
    },
    "Hermit Guide": {
        description: "Along the route, you come across a hermit. Whether helpful or cursing you to your doom, their presence and lifestyle alone give you valuable clues about your surroundings.\n\n+1 Limit and Pool size."
    },
    "A Weak Lead": {
        description: "Not everything can be confirmed before setting out; sometimes, you need to improvise.\n\nAfter this event is chosen, generate a different modifier and roll a d6. On a 1-2, also generate a different event; on a 5-6, generate a different reward.\n\n-2 reward level\n+1d6 reward level."
    },
    "Emergency Detour": {
        description: "It isn't the route that was planned, but if things go south, you can always go north.\n\nIf this event is failed, ignore all penalties and instead flip a coin and move to a different location on the next movement phase:\n\nHeads: Move to an adjacent, parent, or child area at random - rerolling results that are on route.\n\nTails: Move backwards along the route."
    },
    "Too Quiet": {
        description: "Usually, the expedition plan is more like a guiding light than an itinerary. It can be worrisome when nothing is going wrong.\n\nNo effect this month.\n\nGenerate an additional modifier next month on this expedition after the event has been chosen."
    }
};

const rewards = {
    "Supply Cache": {
        description: "Either from locals, a previous expedition, or a natural stockpile, there is a large cache of supplies available.\n\n(Random Supply from the Supply Types Table during generation)."
    },
    "Natural Reserves": {
        description: "The region has bountiful reserves of a tradable commodity.\n\nMake a Reward Check and mark a reserve in this location (if greater).\n\nIf the organization's base location is moved here (by taking all characters and supplies on a one-way expedition), then all organization purchasing checks have a + Reserve Size to their pool and limit sizes."
    },
    "A Map Fragment": {
        description: "An advanced piece of technology displaying a mind-bending multi-dimensional map. You can clearly tell where you are, but the surroundings are exceedingly difficult to identify even when comparing them with your own detailed maps.\n\nIf this expedition returns to base, mark the organization's mapping progress up by a % equal to the number of known locations.\n\nAt 100%, the final location is revealed at random from all existing locations.\n\nIf already at 100%, gain instead:\n\nMultidimensional Compass - Can be consumed to choose a single event, modifier, or reward card when generating events (the random elements are generated first before choosing)."
    },
    "Artifact": {
        description: "An unknown and nearly alien piece of tech.\n\nArtifact Unattuned (Reward # X) - Automatically pass one Pass/Fail check. Once used, the artifact attunes itself to that check type and becomes\n\nArtifact Attuned (Reward Check X) - Adds +X limit on all checks of this type led by this character."
    }
};

const events = {};

module.exports = { events, modifiers, rewards };
