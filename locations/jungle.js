const events = {
    "Climb Through the Clouds": {
        description: "Your best route is a sharp climb through the misty cloud layer of a hill covered in dense vines. Yet somehow, you are so very glad you don't have to go the other way around.\n\nTravel (Body)\n\nPass: The climb takes days just to ascend a few hundred feet. The expedition is exhausted, but the ordeal is behind them.\n\nFail: Days become weeks as you start, fail, and replan the climb multiple times. After enough attempts, you've mapped out the clouded region and finally have a viable path through the easiest route.\n\nExpedition movement delayed 1 month.",
        check: {
            attribute: 'body',
            type: 'Travel',
            fail: {
                delay: 1,
            }
        }
    },
    "River Falls": {
        description: "A lucrative location is spotted at the base of a waterfall, however, your expedition is at the top. Getting down is easy, but coming back up will require extensive preparation.\n\nEngineering (Mind)\n\nPass: A temporary lift is constructed from rope, jungle vines, and logs. The construction is sturdy enough for the times you require it, even if the natural materials will decay quickly.\n\nFail: A series of poorly anchored scaffolding collapses from the falls' strong updrafts while you are working at the base of the falls.\n\nRewards still gained.\n\nInjury Check (all characters)\n\nThe next location on the route can no longer be reached directly from this location for this expedition; a detour must be used."
    },
    "Flash Flood": {
        description: "Before any further journeys can be had, you need to deal with the rain. It has been raining nonstop for a week, and the valley where your camp is set up begins to flood. Hauling your camp out in the rising tide would be impossible, and you are forced to build an impromptu floating camp.\n\nEngineering (Mind)\n\nPass: There isn't much time. You tarp up some of your more buoyant containers and scramble to pack everything else on top. It's a clunky and uncomfortable raft, but it works.\n\nFail: You start to tear down some wood and branches to form the shell of a boat, but before you manage to seal the hull, the water consumes your entire camp. Much will be ruined or simply float away, and there all manner of river creatures starting to collect around you.\n\nPick a supply type from your expedition at random - all of that supply type is lost.\n\n- Body (Beasts) - This check automatically passes if the expedition has no carnivore food supplies.\n\n- Pass: A river shark circles around your floating supplies, dissuaded by a few good smacks or just not finding anything worth eating. They swim away.\n\n- Fail: A river shark begins gorging itself on your meaty supplies. They are not interested in you as a snack - you can tell because they spit out the bites of you they took when checking if you were food.\n\n- Injury Check - 1 randomly selected expedition member."
    },
    "Hostile Locals": {
        description: "A marauding group of locals patrols along your route. Their position is well fortified but also full of supplies.\n\nOptional: Social (Soul)\n\nPass: The locals are not friendly, but trading for your supplies is less risky than fighting for them. You may exchange (Reward) number of your expedition supplies for other supplies at a ratio of 2:1.\n\nFail/Skip: The marauders have what you need, but unfortunately, trade isn't an option with these people. Your only option is force.\n\n- Combat (Body) - If you failed the Social check to get here, -1 to pool and limit.\n\n- Pass: A swift shock and awe-style attack catches the marauder camp off-guard, and they scatter into the jungles.\n\nInjury Check 1 random character.\n\n(Supplies individually selected)\n\n- Fail: The encampment is bigger than it looks. After a stalemate fight breaks out, you realize too late that your expedition is actually deep inside the encampment's traps and defenses. A fighting retreat leaves the expedition battered.\n\n- Injury Check all characters.\n\n- A second Injury Check 1 additional character."
    }
};

const modifiers = {
    "Exotic Flowers": {
        description: "Your route covers an isolated area with a unique species of flower with medicinal properties.\n\nMedicine (Mind)\n\nPass: +1 Injuries Supply.\n\nFail: Injury check for check leader."
    },
    "Turned Around": {
        description: "The jungle can be a confusing place when the sun is blocked by the canopy and every tree starts to look the same.\n\nThe check leader for this event is selected randomly."
    },
    "Friendly Locals": {
        description: "There is a permanent encampment nearby.\n\nIf you have a Primal background character in the expedition - the next purchasing done may be in this location for the expedition instead of at the base location for the organization. If this option is chosen, reduce the number of supplies purchased by half (round up).\n\nIf no expedition characters have a primal background, they offer you a guide to help while you are operating nearby. +1 pool and +1 limit this event."
    },
    "Hanging Canopy": {
        description: "The route takes you through an area where a massive swarm of bats roost. They are harmless critters, right?\n\nStealth (Body)\n\nPass: You quietly pass through the bats' roosting territory and are in awe from the spectacle of the furry brown canopy that stretches for miles.\n\nMorale +1.\n\nFail: You try and pass quietly through the roosting territory, but something spooks the bats, and all at once, they take flight and rain down on you little brown gifts for minutes until the swarm has passed over you.\n\nMorale -1."
    }
};

const rewards = {
    "Natural Abundance": {
        description: "This area contains a plethora of easily accessible and long-lasting fruits.\n\n(Vegetarian Food Supplies)\n\n1 morale for all characters.",
        supplies: ['Vegetarian Diet'],
        moraleAll: 1
    },
    "River Trail": {
        description: "There is a wide and peaceful river heading along your path for a good long stretch.\n\nYou may choose to bypass the next month's events, and separately, you may choose to move 2 locations instead of one."
    },
    "Natural Glue": {
        description: "A sizable ring of mushrooms covering many acres excretes a dense paste that hardens into something like concrete to keep away rival vegetation and capture insects that would feed on the fungi. A valuable construction supply.\n\n(Purchasing Supplies)\n\n(Engineering Supplies)",
        supplies: ['Purchasing', "Engineering"]
    },
    "Abandoned Encampment": {
        description: "An encampment you spot has been reclaimed by the jungle. It looks mostly empty, but there may be some goods left behind.\n\n(Supplies - random type chosen when earned)\n\n(Lifestyle Supplies - Chosen at random when generated)"
    }
};


module.exports = { events, modifiers, rewards };
