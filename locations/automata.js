const events = {
    "An Optimized Vault": {
        description: "Automata love to tinker with their own capabilities, and it is a common practice for them to leave a bountiful reward behind a design or engineering problem lock-box. There is one such box present along your route, but it is overgrown with plants and dirt. No automata has attempted this in ages because the problem has been optimized to its limit. You can't better the automata race, but you could possibly trick the system into opening up anyway.\n\nTechnology (Mind) (x2 Difficulty, x0 Risk)\n\nPass: After many days of investigating the mechanisms, you discover a backdoor into the system where the original builder could put in a specific input to test-open the vault. You never figure out the actual key-phrase, but after some predictive hashing, you manage to spoof it.\n\nFail: Poking around in the inner workings of the vault's personality matrix leaves your expedition more confused than it started. By the end, you are not even sure how the vault ever worked in the first place."
    },
    "War Games": {
        description: "Automata are isolationist by nature, as they are fully self-sufficient even at an individual level. However, they are not aggressive to outsiders such as your expedition who treat them respectfully. A group of Automata warriors in the area have access to a number of treasures left over from failed raids against them. Maybe you could strike a deal.\n\nPick One of: Attempt to barter with the warriors\n\nSocial (Soul) - Combat Supplies may be used for this check as well\n\nPass: You convince the warriors you can make good use of some of the treasures they have little use for themselves and might someday use them against raiders yourself.\n\nFail: The warriors are not interested in equipping outsiders, even allies, which you are not.\n\nOr: Offer to participate in war games as a training day. They agree and offer up a reasonable prize for you to capture, if you can.\n\nCombat (Mind)\n\nPass: Your strategies are highly thought out and well-executed, but the gap between a perfect approach and yours is enough to slip through and claim the prize. The automata thank you for the opportunity.\n\nFail: Your textbook strategies are easily thwarted by the well-practiced warriors. Later, during a social event, you discover the textbook you learned the strategy from is quite literally contained in the warriors' firmware."
    },
    "Constructor Swarm": {
        description: "Through a buzzing cloud of construction automata, your expedition spots a point of interest. They don't seem at all interested in you or your point of interest, but when you approach, the whole swarm stops, forms a protective wall with their bodies, and shoos you out of harm's way as if you were a lost deer. A non-conventional mode of travel or stealth might still allow you to proceed before the swarm builds over the entire area.\n\nChoose Either Of: Travel (Body)\n\nPass: You spend a number of days tunneling under the swarm, making it to your destination unseen.\n\nFail: You spend a number of days tunneling under the swarm, but when you come to the surface off-target in the middle of the construction area, the swarm ushers you out and quickly fills in the structural gap that was your tunnel work.\n\nOr: Technology (Mind)\n\nPass: The automata constructors 'see' one another through a simple radio strobe. After some trial and error, the expedition manages to replicate this signal and pass through seen but unnoticed.\n\nFail: The automata constructors can't herd you if they can't see you. After some major reworking of the gear, the expedition has a rough signal jammer that should produce enough signal noise to pass through unseen. Instead, it acts as a beacon, and the expedition is extracted from the construction zone even farther than usual."
    },
    "Archival Vault": {
        description: "The expedition spots what they think is an easy win, resources right out in the open left abandoned or simply not interesting to the automata of the region. On approach, however, a speaker automata, not much more than a personality matrix and a screen, brightens the area and welcomes you to the archives. You explain why you were interested in the resources, and the speaker seems receptive to providing you a functional replica of the originals.\n\nSpecial: If the reward for this event is not a supply, item, or reasonably an object, re-generate it until it is.\n\nSocial (Soul)\n\nAfter some discussion on your goals and needs, the speaker allows you access to a replica replacement. Treat the reward value for this event as the number of success on the check."
    }
};


const modifiers = {
    "Art and Soul": {
        description: "A traveling troupe of artist automata is available to perform. Their works are legendary, and the expedition could enjoy their works forever. Leaving can leave some feeling empty afterwards, though.\n\nWager 0 to 6 points.\n\nSubtract wager from pool and limit this event.\n\nIf the event is a success, roll a d6. If the roll is greater than or equal to the wager, gain morale equal to the wagered amount."
    },
    "Automata Mega Structure": {
        description: "This specific area is contained partially within a giant building that blurs the boundaries between nature and engineering. The breaks and transitions between natural and synthetic topography can be quite confusing to an evolved mind.\n\n-1 check limit and pool.\n\nUnless the check leader is automata, then +2 check limit and pool."
    },
    "Cipher": {
        description: "This lead was found in a cache of sensor data in an old automata scanning station. With enough time to prepare and study the data, the tasks at hand should be much simpler.\n\nOptional: Delay this event until next month (do not generate events for next month), but any checks for the event are augmented by an additional Technology (Mind) check - adding to the success results."
    },
    "Lab Mice": {
        description: "There is a group of automata researchers following your expedition and logging data. They stay out of the way, and you get the impression they are trying to be neutral observers only. Still, there are subtle tells and glances you can pick up on to gather information about your task from the researchers, even if they aren't intending it.\n\nA Social (Soul) check may be used in place of one other check this event."
    }
};

const rewards = {
    "Replica Operator": {
        description: "A humanoid automata shell with no personality matrix installed. Great for operating in hostile environments.\n\nReplica X - When this character makes injury checks, first make the check with the replica. If the replica makes the check, they were capable of standing in and are destroyed instead."
    },
    "Personality Matrix": {
        description: "An advanced general intelligence chip with an attached sensor suite. Able to provide dynamic field data analysis.\n\nAI - +2 pool to mind checks, +1 pool to soul checks.\n\nCan be combined with a Replica to create a new character with the following properties:\n\n- Background Cyber (Automata)\n- Diet none\n- Lifestyle none\n- All attributes are 3d6 DL\n- Other traits: Repairable - all injury checks against this character may be made as an Engineering (Mind) Check by any expedition member\n- Other traits: Robotic - injury supplies cannot be applied to this character\n- Other traits: Configurable - while not on an expedition, this character may swap any 2 attributes"
    },
    "Skill Module": {
        description: "A robotics kit complete with sensors and actors that is meant to be built into an automata. It can be repurposed to be used as a powerful tool or assistant for its intended task.\n\nSkill Module (Reward #, Random Supply check type) +2 pool and limit to matching check types.\n\nMay only be owned by an Automata character, but carried and used by anyone.\n\nWhile at the base location, characters can un-own this item."
    },
    "S.A.P.": {
        description: "Self-Arranging Printer goop can arrange itself into complex patterns within a holographic field before undergoing an exothermic expansion and solidifying into solid alloy structure. Not suitable for field printing, this material is still invaluable in urban infrastructure.\n\n(Purchasing Supplies)",
        supplies: ['Purchasing']
    }
};


module.exports = { events, modifiers, rewards };
