
const events = {
    "Blinking Neon": {
        description: "A small cluster of neon advertisements is still occasionally flickering weakly in an otherwise dead concrete jungle. If there is power, there might be something useful at the source, if only scraps left by scavengers before.\n\nEngineering (Mind)\n\nPass: You trace a live wire up a decrepit tower and find a collection of solar panels, one of which is still barely functional. You also spot something interesting nearby from the vantage point.\n\nFail: The power signal is too erratic for you to trace it anywhere without tapping into it directly, which blows the circuit and neon signs in a colorful, and painful, flash.\n\nInjury Check (Check Leader).",
        check: {
            attribute: 'mind',
            type: 'Engineering',
            fail: {
                injuryCheckLeader: true,
            }
        }
    },
    "Leaning Tower of Pizza": {
        description: "A novelty rotating pizza at the top of a tall tower is still spinning in silence. Many of the floors of the tower below it are rotted away to just the framework, so to reach the restaurant would require a dangerous climb, but the place looks relatively untouched by the devastation.\n\nTravel (Body)\n\nPass: A single climber is sent up to toss down anything they find of use and report back. After a few near misses on the way up and down, they return safely after having found a refrigerated cache of frozen pizzas and some other interesting sights.\n\n(Vegetarian Diet Supplies)\n\nFail: Halfway up the climb, the climber's rope becomes frayed on the exposed metal frameworks and they tumble down the tower girders in a series of small but risky falls.\n\nInjury Check (Check Leader)."
    },
    "Oh Grate": {
        description: "Searching through a hill of debris that used to be a tower, the expedition spots something interesting in the sewers underneath through a mostly covered grate. If you can navigate to that part of the sewers from another entrance, it will certainly be worth it.\n\nTravel (Mind)\n\nPass: The expedition explores a number of winding entrances, collapsed sections, and drain pipes but eventually makes it to the location.\n\nFail: The expedition gets turned around in the labyrinthian sewers, and before they find the way out, they are forced to make camp. In the night, a swarm of rats rushes into camp.\n\n- Beasts (Soul)\n\n- Pass: The expedition does their best to climb to high ground and let the swarm pass unhindered. Still, some supplies are lost. Lose Risk/2 supplies at random.\n\n- Fail: The expedition attempts to barricade the camp from the swarm, but the rats still find a way in and are now trapped inside the barricades. Lose supplies at random. Injury Check (1 Random Character)."
    },
    "Hacking the Cloud": {
        description: "There is a data cache visible near the top of a tower, it looks like a civil data center and could have invaluable information on local points of interest. However, the servers you can see are literally dangling from their power cords dozens of stories in the air.\n\nTravel (Body)\n\nPass: A single brave tech climbs up the tower to the lower data port they can find.\n\n- Technology (Mind) - This check must use the same check leader as the previous check.\n\n- Pass: The tech successfully accesses the systems and with the combined data and view, can easily map out a safe route to some points of interest.\n\n- Fail: The tech safely descends from the tower but sadly has no useful data to report.\n\nFail: A single brave tech tries the ascent to access a data port but makes an unplanned descent slowed only by the 'vines' of data cabling.\n\nInjury Check (Check Leader)."
    }
};


const modifiers = {
    "Free Parking": {
        description: "This area's skyscrapers still have some of their surrounding infrastructure intact. A roof and 4 walls are a rare accommodation on the expedition.\n\nRefund all lifestyle supplies spent by the expedition this month."
    },
    "Dizzying Heights": {
        description: "Your route either takes you up or under a particularly tall skyscraper that is clearly about to collapse.\n\n+2 Risk."
    },
    "City Mold": {
        description: "Your route takes you near a grocer or food warehouse that has completely molded over, even into the super-structure itself. Anything that could survive the formerly hi-tech sterilization is bound to be a potent and aggressive biological agent.\n\n* Diet (Body) - If this check would not pass, you must spend supplies until it does, or food supplies run out."
    },
    "Cryo Chambers": {
        description: "A self-contained and unused cryo chamber is found along this route. The expedition may optionally:\n\nA) Delay a month without paying upkeep.\nB) Pause here until another expedition arrives.\nC) Loot the nutrient fluid; no food supplies are needed for the rest of the expedition, but -1 morale per month to all characters."
    }
};


const rewards = {
    "Satellite Uplink": {
        description: "A geosynchronous satellite originally built for communication is still online above the city ruins in the area and can be redirected, though the orbital change will quickly cut the line of sight uplink.\n\nPick One:\n\n- Increase Map% by 1/5th reward value%.\n- Reveal an unknown adjacent location.\n- Crash the satellite near base for parts, gaining (Technology Supplies) for the organization (NOT the expedition)."
    },
    "Raw Building Materials": {
        description: "Towers are sturdy things, even when they fall over they can be a rich supply of building materials.\n\n(2x Engineering Supplies).",
        supplies: ["Engineering", "Engineering"]
    },
    "Hi Tech Gear": {
        description: "A cache of high-tech equipment of all shapes and kinds. Most of it is broken, but there might still be something worth salvaging.\n\n(Technology Supplies)\n\nCheck Bonus Item +X (Reward/2 check X, random supply type chosen when acquired)."
    },
    "Panic Room": {
        description: "There is a sealed panic room visible in one of the nearby towers. Roll 1d6 on opening it:\n\n1 - Military panic room - (Combat Supplies).\n2 - Emergency Bunker - (Vegetarian Diet Supplies).\n3 - Operating Room - (Injury Supplies).\n4 - Virtual Reality Pod - (Technology Supplies) +1 Morale.\n5 - Treasure Room - (Purchasing Supplies x2).\n6 - Cryo Chamber - Expedition gains a character with 1d6 for all attributes, cyber background, and other trait: Closed System Engineering - when at the base location, this character reduces supply decay for a single supply type by 1 per month."
    }
};


module.exports = { events, modifiers, rewards };