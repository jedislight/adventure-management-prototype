const { d, shuffle, sample } = require('./utility');
let _id = 0;
function _getNextId() {
    return ++_id;
}
class Location {
    constructor(parent, slot) {
        this.size = parent ? parent?.size - 1 : 4;

        this.within = parent ? parent : null;
        this.contains = [];
        for (let i = 0; i < this.size; ++i) this.contains.push(null);

        if (parent) {
            parent.contains[slot - 1] = this;
        }

        const baseDR = sample([[4, 4], [3, 5], [5, 3]]);
        this.difficulty = parent ? parent.difficulty - 2 + d(6) : baseDR[0];
        this.risk = parent ? parent.risk - 2 + d(6) : baseDR[1];
        this.reward = Math.floor((this.difficulty + this.risk) / 2.0);

        this.trait = parent ? sample(locationTraits) : locationTraits[d(6) - 1];
        this.inheritedTraits = parent ? parent.inheritedTraits.slice().concat([parent.trait]) : [];

        this.id = parent ? parent.id + slot : "R";
        this.name = parent ? this.id : "World Root";
    }

    toString() {
        return `--${this.name}(${this.id})--
Within: ${this.within?.name || '-'}
Contains: ${this.contains.map(l => `${l ? l.name : '-'}`)}
Difficulty: ${this.difficulty}
Risk: ${this.risk}
Reward: ${this.reward}
Trait: ${this.trait}
Inherited Traits: ${JSON.stringify(this.inheritedTraits)}`
    }
}

class Character {
    constructor(name) {
        this.name = name || "Name";
        this.inOrganization = false;
        this.body = new Attribute();
        this.mind = new Attribute();
        this.soul = new Attribute();
        this.coreTraits = [null, null, null, null, null, null, null, null];
        this.otherTraits = [];
        this.items = [null, null, null];
        this.teamwork = {};
        this.presentingTrait = 0;
        this.id = _getNextId();
        this.morale = 0;

        let attributes = shuffle([this.body, this.mind, this.soul]);
        attributes[0].die = 3;
        attributes[1].die = 2;
        attributes[2].die = 1;

        const backgroundIndex = d(8) - 1;
        this.presentingTrait = backgroundIndex;
        const selectedBackground = sample(coreTraits.Background);
        this.coreTraits[backgroundIndex] = `Background - ${selectedBackground}`;
        switch (selectedBackground) {
            case "Riftwalker":
                this.soul.die += 1;
                this.soul.dropLowest = true;
                break;

            case "Cyber":
                this.mind.die += 1;
                this.mind.dropLowest = true;
                break;

            case "Primal":
                this.body.die += 1;
                this.body.dropLowest = true;
                break;

            default: throw `Unknown background ${selectedBackground}`;
        }
    }

    modifyTeamwork(other, amount) {
        this.teamwork[other.id] = (this.teamwork[other.id] || 0) + amount;
        other.teamwork[this.id] = (other.teamwork[this.id] || 0) + amount;
    }

    toString() {
        return `--${this.name}--
Body ${this.body.toString()}
Mind ${this.mind.toString()}
Soul ${this.soul.toString()}
Morale ${this.morale}

Core Traits: 
\t1: ${this.coreTraits[0] || " ? "}
\t2: ${this.coreTraits[1] || " ? "}
\t3: ${this.coreTraits[2] || " ? "}
\t4: ${this.coreTraits[3] || " ? "}
\t5: ${this.coreTraits[4] || " ? "}
\t6: ${this.coreTraits[5] || " ? "}
\t7: ${this.coreTraits[6] || " ? "}
\t8: ${this.coreTraits[7] || " ? "}
Other Traits ${JSON.stringify(this.otherTraits)}
Items ${JSON.stringify(this.items)}
Teamwork ${JSON.stringify(this.teamwork)}
Presenting Trait ${this.coreTraits[this.presentingTrait]}`;
    }
}

class Expedition {
    constructor() {
        this.name = "Expedition Name"
        this.route = []
        this.members = []
        this.goals = []
        this.supplies = {}
        this.id = _getNextId();
        this.location = null;
        this.delay = 0;
    }

    toString() {
        return `--${this.name}--
Route: ${JSON.stringify(this.route)}
Members: ${JSON.stringify(this.members.map(m => m.name))}
Supplies: ${JSON.stringify(this.supplies)}
Location: ${this.location.name}
${this.delay ? `Delayed: ${this.delay} months` : ''}
`
    }
};

const coreTraits = {
    "Background": ["Cyber", "Primal", "Riftwalker"],
    "Goal": ["Adventure", "Artifact", "Civic", "Explore"],
    "Outlook": ["Optimist", "Pessimist"],
    "Planning": ["Improviser", "Planner"],
    "Preservation": ["Exploit", "Flexible", "No Trace"],
    "Lifestyle": ["Lavish", "Survivalist"],
    "Risk": ["Adverse", "Daredevil"],
    "Diet": ["Carnivore", "Eccentric", "Flexible", "Vegetarian"]
};

class Attribute {
    constructor() {
        this.value = null;
        this.die = 3;
        this.dropLowest = false;
        this.id = _getNextId();
    }

    getValue() {
        if (this.value == null) {
            let roll = [];
            for (let i = 0; i < this.die; ++i) {
                roll.push(d(6));
            }
            roll.sort();
            if (this.dropLowest) {
                roll.shift();
            }

            this.value = roll.reduce((a, b) => a + b)
        }

        return this.value;
    }

    toString() {
        return `${this.value == null ? '?' : this.value} (${this.die}d6${this.dropLowest ? " DL" : ""})`;
    }
}

const supplyTypes = [
    "Purchasing",
    "Character Scouting",
    "Vegetarian Diet",
    "Carnivore Diet",
    "Lavish Lifestyle",
    "Survivalist Lifestyle",
    "Beasts",
    "Travel",
    "Stealth",
    "Injury",
    "Combat",
    "Engineering",
    "Social",
    "Technology",
    "Mysticism"
];

const locationTraits = [
    "Dinosaurs",
    "Jungle",
    "Automata",
    "Skyscraper Ruins",
    "Aetherials",
    "Rift Wastes",
]

const eventsByTrait = {
    "Dinosaurs": require('./locations/dinosaurs').events,
    "Jungle": require('./locations/jungle').events,
    "Automata": require('./locations/automata').events,
    "Skyscraper Ruins": require('./locations/skyscraperRuins').events,
    "Aetherials": require('./locations/aetherials').events,
    "Rift Wastes": require('./locations/riftWastes').events,
}

const modifiersByTrait = {
    "Dinosaurs": require('./locations/dinosaurs').modifiers,
    "Jungle": require('./locations/jungle').modifiers,
    "Automata": require('./locations/automata').modifiers,
    "Skyscraper Ruins": require('./locations/skyscraperRuins').modifiers,
    "Aetherials": require('./locations/aetherials').modifiers,
    "Rift Wastes": require('./locations/riftWastes').modifiers,
    "Global": require('./locations/global').modifiers,
}

const rewardsByTrait = {
    "Dinosaurs": require('./locations/dinosaurs').rewards,
    "Jungle": require('./locations/jungle').rewards,
    "Automata": require('./locations/automata').rewards,
    "Skyscraper Ruins": require('./locations/skyscraperRuins').rewards,
    "Aetherials": require('./locations/aetherials').rewards,
    "Rift Wastes": require('./locations/riftWastes').rewards,
    "Global": require('./locations/global').rewards,
}

class Encounter {
    constructor(location) {
        const e = sample(eventsByTrait[sample([location.trait].concat(location.inheritedTraits))]);
        this.event = {
            name: e[0],
            ...e[1]
        }

        const m = sample(modifiersByTrait[sample([location.trait].concat(location.inheritedTraits))]);
        this.modifier = {
            name: m[0],
            ...m[1]
        }

        const r = sample(rewardsByTrait[sample([location.trait].concat(location.inheritedTraits))]);
        this.reward = {
            name: r[0],
            ...r[1]
        }
    }

    toString() {
        return `${this.event.name}-${this.modifier.name}-${this.reward.name}`;
    }

    longString() {
        return `Event: ${this.event.name}
${this.event.description}

Modifier: ${this.modifier.name}
${this.modifier.description}

Reward: ${this.reward.name}
${this.reward.description}
`
    }
}

module.exports = { Character, Expedition, Location, coreTraits, supplyTypes, Encounter, eventsByTrait, modifiersByTrait, rewardsByTrait };