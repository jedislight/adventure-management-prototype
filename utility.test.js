const { d, shuffle, sample } = require('./utility');

test('d in range', () => {
    for (let i = 0; i < 1000; ++i) {
        const roll = d(8)
        expect(roll).toBeLessThanOrEqual(8);
        expect(roll).toBeGreaterThanOrEqual(1);
    }
});

test('sample coverage', () => {
    const original = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const results = [];
    const attempts = 1000;
    for (let i = 0; i < attempts; ++i) {
        results.push(sample(original));
    }

    expect(original.find(e => !results.includes(e))).toBeUndefined();
});