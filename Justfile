set windows-shell := ["cmd.exe", "/c"]

init:
    npm install

run:
    npm install
    npm run start

test:
    npm install
    npm run test