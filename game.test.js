const { Game } = require('./game');
const { Location, Expedition, Character, coreTraits, supplyTypes, Encounter, eventsByTrait, modifiersByTrait, rewardsByTrait } = require('./data');
const { sample } = require('./utility');

const commands = [
    "",
    "y",
    "n",
    "skip",
    "1",
    "2",
    "3",
    "4",
    "random",
    "body",
    "mind",
    "soul",
    "unroute",
    "route R1",
    "pack Beasts 1",
    "unpack Beasts 1",
    "pack Carnivore Diet 1",
    "pack Vegetarian Diet 1",
    "pack Lavish Lifestyle 1",
    "pack Survivalist Lifestyle 1",
    "Beasts 1",
    "Carnivore Diet 1",
    "Vegetarian Diet 1",
    "Lavish Lifestyle 1",
    "Survivalist Lifestyle 1",
    "new",
    "add Character 0",
    "remove Character 0",
    "embark",
    "cancel",
    "locations",
    "characters",
    "supplies",
    "expeditions"
];

function getAllEncounterParts(xByTrait) {
    let result = [];
    Object.keys(xByTrait).forEach(locationTrait => {
        Object.entries(xByTrait[locationTrait]).forEach(([name, x]) => {
            result.push({
                name: name,
                ...x
            })
        });
    })

    return result;
}

describe('all encounter parts implemented', () => {
    const events = getAllEncounterParts(eventsByTrait)
    const modifiers = getAllEncounterParts(modifiersByTrait);
    const rewards = getAllEncounterParts(rewardsByTrait);

    const allParts = [...events, ...modifiers, ...rewards];
    allParts.forEach(ep => {
        test(ep.name, () => {
            expect(Object.keys(ep).length).toBeGreaterThan(2);
        })
    })
})

test('check teamwork at base and expeditions', () => {
    let game = new Game();
    game.characters.push(new Character());
    game.characters[0].modifyTeamwork(game.characters[1], 1000);

    expect(game.characters[0].teamwork[game.characters[1].id]).toBe(1000);

    // check teamwork at base
    game.checkConfig = {
        lead: game.characters[0],
        attribute: 'mind',
        supply: 'Beasts',
        pool: game.characters[0].mind.getValue(),
        limit: game.characters[0].mind.getValue()
    }
    game.replaceState(game.stateMonthBegin)
    game.pushState(game.stateCheck);
    game.getNext(game.NO_PROMPT);
    expect(game.checkConfig.limit).toBeGreaterThan(100);

    // check teamwork doesn't count split members
    game.expeditions.push(new Expedition());
    game.expeditions[0].members.push(game.characters[0]);

    game.checkConfig = {
        lead: game.characters[0],
        attribute: 'mind',
        supply: 'Beasts',
        pool: game.characters[0].mind.getValue(),
        limit: game.characters[0].mind.getValue()
    }
    game.replaceState(game.stateMonthBegin)
    game.pushState(game.stateCheck);
    game.getNext(game.NO_PROMPT);
    expect(game.checkConfig.limit).toBeLessThan(100);

    // check teamwork works for expeditions
    game.expeditions[0].members.push(game.characters[1]);
    game.checkConfig = {
        lead: game.characters[0],
        attribute: 'mind',
        supply: 'Beasts',
        pool: game.characters[0].mind.getValue(),
        limit: game.characters[0].mind.getValue()
    }
    game.replaceState(game.stateMonthBegin)
    game.pushState(game.stateCheck);
    game.getNext(game.NO_PROMPT);
    expect(game.checkConfig.limit).toBeGreaterThan(100);
});

test('check relationship building', () => {
    let game = new Game();
    game.characters.push(new Character());
    game.characters[0].coreTraits[0] = "TEST - TRAIT";
    game.characters[1].coreTraits[0] = "TEST - TRAIT";
    game.characters[0].presentingTrait = 0;
    game.characters[1].presentingTrait = 0;

    game.pushState(game.stateUpdateRelationships);
    game.getNext(game.NO_PROMPT);

    expect(game.characters[0].teamwork[game.characters[1].id]).toBe(1);
});

test('check relationship decay', () => {
    let game = new Game();
    game.characters.push(new Character());
    game.characters[0].coreTraits[0] = "TEST - TRAIT";
    game.characters[1].coreTraits[0] = "TEST - OPPOSE";
    game.characters[0].presentingTrait = 0;
    game.characters[1].presentingTrait = 0;

    game.pushState(game.stateUpdateRelationships);
    game.getNext(game.NO_PROMPT);

    expect(game.characters[0].teamwork[game.characters[1].id]).toBe(-1);
});

test('check relationship neutral', () => {
    let game = new Game();
    game.characters.push(new Character());
    game.characters[0].coreTraits[0] = "TEST - TRAIT";
    game.characters[1].coreTraits[0] = "DIFFERENT - OPPOSE";
    game.characters[0].presentingTrait = 0;
    game.characters[1].presentingTrait = 0;

    game.pushState(game.stateUpdateRelationships);
    game.getNext(game.NO_PROMPT);

    expect(game.characters[0].teamwork[game.characters[1].id] || 0).toBe(0);
});

test('run all events', () => {
    let game = new Game();
    game.characters.push(new Character());
    let exp = new Expedition();
    exp.location = game.locationRoot;
    exp.members.push(game.characters[0]);
    exp.members.push(game.characters[1]);
    exp.members[0].mind.value = 100;
    exp.members[0].body.value = 100;
    exp.members[0].soul.value = 100;

    exp.members[1].mind.value = 2;
    exp.members[1].body.value = 2;
    exp.members[1].soul.value = 2;
    game.currentExpedition = exp;
    game.expeditions.push(exp);

    const allEvents = getAllEncounterParts(eventsByTrait);
    const allModifiers = [getAllEncounterParts(modifiersByTrait)[0]];
    const allRewards = [getAllEncounterParts(rewardsByTrait)[0]];
    runAllEncounterPermutations(allEvents, allModifiers, allRewards, game);
});

test('run all modifiers', () => {
    let game = new Game();
    game.characters.push(new Character());
    let exp = new Expedition();
    exp.location = game.locationRoot;
    exp.members.push(game.characters[0]);
    exp.members.push(game.characters[1]);
    exp.members[0].mind.value = 100;
    exp.members[0].body.value = 100;
    exp.members[0].soul.value = 100;

    exp.members[1].mind.value = 2;
    exp.members[1].body.value = 2;
    exp.members[1].soul.value = 2;
    game.currentExpedition = exp;
    game.expeditions.push(exp);

    const allEvents = [getAllEncounterParts(eventsByTrait)[0]];
    const allModifiers = getAllEncounterParts(modifiersByTrait);
    const allRewards = [getAllEncounterParts(rewardsByTrait)[0]];
    runAllEncounterPermutations(allEvents, allModifiers, allRewards, game);
});

test('run all rewards', () => {
    let game = new Game();
    game.characters.push(new Character());
    let exp = new Expedition();
    exp.location = game.locationRoot;
    exp.members.push(game.characters[0]);
    exp.members.push(game.characters[1]);
    exp.members[0].mind.value = 100;
    exp.members[0].body.value = 100;
    exp.members[0].soul.value = 100;

    exp.members[1].mind.value = 2;
    exp.members[1].body.value = 2;
    exp.members[1].soul.value = 2;
    game.currentExpedition = exp;
    game.expeditions.push(exp);

    const allEvents = [getAllEncounterParts(eventsByTrait)[0]];
    const allModifiers = [getAllEncounterParts(modifiersByTrait)[0]];
    const allRewards = getAllEncounterParts(rewardsByTrait);
    runAllEncounterPermutations(allEvents, allModifiers, allRewards, game);
});

test('stress', () => {
    let game = new Game();
    const iterations = 4000;

    for (let i = 0; i < iterations; ++i) {
        game.getNext(sample(commands));
    }
});

function runAllEncounterPermutations(allEvents, allModifiers, allRewards, game) {
    allEvents.forEach(event => {
        allModifiers.forEach(modifier => {
            allRewards.forEach(reward => {
                game.state = [game.stateMonthBegin];
                game.currentEncounter = new Encounter(game.locationRoot);
                game.currentEncounter.event = event;
                game.currentEncounter.modifier = modifier;
                game.currentEncounter.reward = reward;
                console.log(game.currentEncounter.toString());
                game.pushState(game.stateExpeditionEncounterResolution);
                while (game.state.length > 1) {
                    if (game.state.includes(game.stateExpeditionChooseEncounterLeader)) {
                        game.getNext('1');
                    } else {
                        game.getNext(sample(commands));
                    }
                }

                game.currentEncounter = new Encounter(game.locationRoot);
                game.currentEncounter.event = event;
                game.currentEncounter.modifier = modifier;
                game.currentEncounter.reward = reward;
                game.state = [game.stateMonthBegin];
                game.pushState(game.stateExpeditionEncounterResolution);
                while (game.state.length > 1) {
                    if (game.state.includes(game.stateExpeditionChooseEncounterLeader)) {
                        game.getNext('2');
                    } else {
                        game.getNext(sample(commands));
                    }
                }
            });
        });
    });
}
