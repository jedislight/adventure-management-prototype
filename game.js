const { Location, Expedition, Character, coreTraits, supplyTypes, Encounter } = require('./data');
const { d, shuffle, sample } = require('./utility');

class Game {
    constructor() {
        this.locationRoot = new Location();
        this.locations = [this.locationRoot];
        this.characters = [new Character("Character 0")];
        this.expeditions = [];
        this.supplies = {
            "Carnivore Diet": 10,
            "Vegetarian Diet": 10,
            "Character Scouting": 10,
            "Lavish Lifestyle": 10,
            "Survivalist Lifestyle": 10,
        };

        this.state = [this.stateMonthBegin];
        this.NO_PROMPT = "$$$$"
        this.CONTINUE = "<Continue>"
        this.lastPrompt = "";
        this.month = -1;
        this.expeditionIndex = -1;
    }

    getNext(input) {
        const baseResult = this.stateBase(input);
        if (baseResult != null) {
            console.log('\n' + baseResult);
            return this.lastPrompt;
        }

        do {
            console.log('');
            this.lastPrompt = this.state[this.state.length - 1].call(this, input);
            input = this.NO_PROMPT;
        } while (this.lastPrompt == this.NO_PROMPT);
        return this.lastPrompt;
    }

    pushState(state) {
        this.state.push(state);
    }

    popState() {
        this.state.pop();
    }

    replaceState(state) {
        this.popState();
        this.pushState(state);
    }

    stateMonthBegin() {
        this.month += 1;
        const year = Math.floor(this.month / 12);
        const monthOfYear = this.month - year * 12;
        const monthString = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][monthOfYear];
        console.log(`Year ${year + 1} - ${monthString}`);
        this.replaceState(this.statePresentTraits);
        return this.CONTINUE;
    }

    setNewPresentingTraitFor(character) {
        character.presentingTrait = Math.min(d(8), d(8)) - 1;
        if (character.coreTraits[character.presentingTrait] == null) {
            this.fillCoreTraitSlot(character, character.presentingTrait);
        }

        console.log(`${character.name} is presenting trait: ${character.coreTraits[character.presentingTrait]}`);
    }

    fillCoreTraitSlot(character, slotNumber) {
        let traits = Object.keys(coreTraits);
        for (let t in character.coreTraits) {
            const trait = character.coreTraits[t];
            if (trait == null) continue;
            const traitType = trait.split(' - ')[0];
            const index = traits.indexOf(traitType);
            traits.splice(index, 1);
        }

        character.coreTraits[slotNumber] = traits[Math.floor(Math.random() * traits.length)];
        const valueOptions = coreTraits[character.coreTraits[slotNumber]];
        character.coreTraits[slotNumber] += ' - ' + valueOptions[Math.floor(Math.random() * valueOptions.length)]

        console.log(`Discovered: ${character.name}'s trait ${slotNumber + 1} is ${character.coreTraits[slotNumber]}`);
    }

    statePresentTraits() {
        for (let i in this.characters) {
            const character = this.characters[i];
            this.setNewPresentingTraitFor(character);
        }
        this.replaceState(this.stateOranizationConsumption);
        return this.CONTINUE;
    }

    getBaseCharacters() {
        let result = [];
        for (let c in this.characters) {
            let atBase = true;
            const character = this.characters[c];
            for (let e in this.expeditions) {
                const expedition = this.expeditions[e];
                if (expedition.members.includes(character)) {
                    atBase = false;
                }
            }

            if (atBase) {
                result.push(character);
            }
        }

        return result;
    }

    stateSupplyPurchasingCharacterChoice(input) {
        const charactersAtBaseLocation = this.getBaseCharacters();
        if (charactersAtBaseLocation.length == 0) {
            console.log("No characters available to purchase supplies at the base location");
            this.replaceState(this.stateCharacterScouting);
            return this.CONTINUE;
        }

        const matchedCharacter = this.matchInputToCharacterList(input, charactersAtBaseLocation);
        if (matchedCharacter) {
            this.replaceState(this.stateSupplyPurchasing);
            this.pushState(this.stateCheck);
            this.checkConfig = {
                lead: matchedCharacter,
                attribute: 'mind',
                supply: 'Purchasing',
                pool: matchedCharacter.mind.getValue(),
                limit: matchedCharacter.mind.getValue()
            }
            return this.NO_PROMPT;
        }

        console.log(this.getCharacterListDisplay(charactersAtBaseLocation, (c) => `Mind ${c.mind.toString()}`));
        return "Which character should try and purchase supplies?";
    }

    getCharacterListDisplay(list, detailFunc) {
        let result = "";
        for (let c = 0; c < list.length; ++c) {
            const character = list[c];
            const detail = detailFunc(character);
            result += `${c + 1} - ${character.name} (${detail})\n`;
        }

        return result;
    }

    matchInputToCharacterList(input, list) {
        const index = parseInt(input);
        if (isNaN(index)) {
            return list.find(c => c.name == input);
        } else {
            return list[index - 1];
        }
    }

    stateSupplyPurchasing(input) {
        const tokens = input.split(' ');
        const amount = parseInt(tokens.pop());
        const supplyType = tokens.join(' ');

        if (!supplyTypes.includes(supplyType)) {
            console.log(`Unknown Supply Type ${supplyType}, must be one of ${JSON.stringify(supplyTypes)}`);
        } else if (!isNaN(amount) && amount <= this.checkConfig.successes) {
            console.log(`Purchasing ${amount} ${supplyType} Supplies`);
            this.checkConfig.successes -= amount;
            this.supplies[supplyType] = amount + (this.supplies[supplyType] || 0);
        }

        if (this.checkConfig.successes === 0) {
            this.replaceState(this.stateCharacterScouting);
            console.log("Purchasing Complete")
            return this.CONTINUE;
        } else {
            return `What supplies would you like to purchase? (${this.checkConfig.successes} remain)`;
        }
    }

    stateCheck(input) {
        const checkConfig = this.checkConfig;
        this.inlineCheck(checkConfig);

        const spend = parseInt(input);
        if (!isNaN(spend)) {
            if (this.supplies[this.checkConfig.supply] >= spend) {
                this.supplies[this.checkConfig.supply] -= spend;
                this.checkConfig.successes += spend;
            }
        }

        console.log(`Check: ${this.checkConfig.supply}(${this.checkConfig.attribute}) by ${this.checkConfig.lead.name}
Limit: ${this.checkConfig.limit}
Pool: ${this.checkConfig.pool}
Roll: ${this.checkConfig.roll}
Successes: ${this.checkConfig.successes}`);

        if (["done", "no", "0"].includes(input)) {
            this.popState();
            return this.NO_PROMPT;
        } else if (this.supplies[this.checkConfig.supply]) {
            return `How many ${this.checkConfig.supply} Supplies to spend? (You have ${this.supplies[this.checkConfig.supply]})`;
        } else {
            this.popState();
            return this.NO_PROMPT;
        }
    }

    inlineCheck(checkConfig) {
        if (!checkConfig.roll) {
            checkConfig.limit += this.getTeamworkFor(checkConfig.lead);
            checkConfig.pool += checkConfig.lead?.morale || 0;
            checkConfig.roll = [];
            for (let i = 0; i < checkConfig.pool; ++i) {
                checkConfig.roll.push(d(6));
            }
            checkConfig.roll.sort();
            checkConfig.successes = 0;
            let sum = 0;
            for (let i = 0; i < checkConfig.roll.length; ++i) {
                let die = checkConfig.roll[i];
                if (sum + die <= checkConfig.limit) {
                    checkConfig.successes += 1;
                    sum += die;
                }
            }
        }

        return checkConfig;
    }

    stateOranizationConsumption() {
        for (let supplyType in this.supplies) {
            const original = this.supplies[supplyType];
            const consumption = Math.ceil(original / 10.0);
            const newValue = original - consumption;
            console.log(`${supplyType} consumption for the month is ${consumption} (new total of ${newValue})`);
            this.supplies[supplyType] = newValue;
        }

        this.replaceState(this.stateSupplyPurchasingCharacterChoice);
        return this.CONTINUE;
    }

    stateCharacterScouting(input) {
        if (!this.supplies["Character Scouting"]) {
            console.log("No Character Scouting Supplies, cannot scout this month.");
            this.replaceState(this.stateProgressExpeditions);
            return this.CONTINUE;
        }

        if (input == "skip") {
            this.replaceState(this.stateProgressExpeditions);
            return this.CONTINUE;
        }

        let list = this.getBaseCharacters();
        if (list.length == 0) {
            console.log("No characters at base location, cannot scout this month");
            this.replaceState(this.stateProgressExpeditions);
            return this.CONTINUE;
        }

        const selectedCharacter = this.matchInputToCharacterList(input, list);
        if (!selectedCharacter) {
            const displayList = this.getCharacterListDisplay(list, c => `Soul ${c.soul.toString()}`);
            console.log(displayList);
            console.log('skip');
            return 'Which character should do character scouting(Costs 1 Character Scouting Supply)?';
        }

        this.characterScout = selectedCharacter;
        this.replaceState(this.characterScoutTargetSelectionPhase);
        this.supplies['Character Scouting'] -= 1;
        return this.NO_PROMPT;
    }

    characterScoutTargetSelectionPhase(input) {
        if (input == 'new') {
            this.scoutedCharacter = new Character(`Character ${this.month + 1}`);
            this.characters.push(this.scoutedCharacter);
            return this.startDiscoveryPhase();
        }

        let list = this.getBaseCharacters();
        //remove character Scout from list
        const index = list.indexOf(this.characterScout);
        list.splice(index, 1);

        const selectedCharacter = this.matchInputToCharacterList(input, list);
        if (!selectedCharacter) {
            const displayList = this.getCharacterListDisplay(list, c => `Soul ${c.soul.toString()}`);
            console.log(displayList);
            console.log('new');
            return 'Which character should be scouted?';
        }

        this.scoutedCharacter = selectedCharacter;
        return this.startDiscoveryPhase();
    }

    startDiscoveryPhase() {
        this.replaceState(this.stateCharacterScoutingDiscoveryPhase);
        this.pushState(this.stateCheck);
        this.checkConfig = {
            lead: this.characterScout,
            attribute: 'soul',
            limit: this.characterScout.soul.getValue(),
            pool: this.characterScout.soul.getValue(),
            supply: "Charcter Scouting"
        };
        return this.NO_PROMPT;
    }

    stateCharacterScoutingDiscoveryPhase(input) {
        console.log(this.scoutedCharacter.toString());
        if (this.checkConfig.successes == 0 ||
            (this.scoutedCharacter.mind.value != null &&
                this.scoutedCharacter.body.value != null &&
                this.scoutedCharacter.soul != null &&
                !this.scoutedCharacter.coreTraits.includes(null))) {

            console.log(`Character Scouting complete`);
            this.replaceState(this.stateProgressExpeditions);
            return this.CONTINUE;
        }

        switch (input) {
            case 'body': return this.scoutAttribute('body');
            case 'mind': return this.scoutAttribute('mind');
            case 'soul': return this.scoutAttribute('soul');
            case '1': return this.scoutTrait(0);
            case '2': return this.scoutTrait(1);
            case '3': return this.scoutTrait(2);
            case '4': return this.scoutTrait(3);
            case '5': return this.scoutTrait(4);
            case '6': return this.scoutTrait(5);
            case '7': return this.scoutTrait(6);
            case '8': return this.scoutTrait(7);
            case 'random': return this.scoutRandomTrait();
        }

        return `
Please select what to scout (${this.checkConfig.successes} points left)
1 point - (body), (mind), (soul)
1 point - (random) trait. NOTE trait 2d8 take lowest, known traits still spend points
1-8 points - trait slot (1-8)
`
    }

    scoutAttribute(attributeString) {
        if (this.scoutedCharacter[attributeString].value == null) {
            this.checkConfig.successes -= 1;
            this.scoutedCharacter[attributeString].getValue();
            return this.NO_PROMPT;
        } else {
            console.log(`${attributeString} already known`);
            return this.CONTINUE;
        }
    }

    scoutTrait(traitIndex) {
        const cost = traitIndex + 1;
        if (cost > this.supplies["Character Scouting"]) {
            console.log(`Cannot scout trait slot ${traitIndex + 1}, not enough success left (${this.checkConfig.successes}/${cost})`)
            return this.CONTINUE;
        }

        if (this.scoutedCharacter.coreTraits[traitIndex] == null) {
            this.checkConfig.successes -= cost;
            this.fillCoreTraitSlot(this.scoutedCharacter, traitIndex);
            return this.NO_PROMPT;
        } else {
            console.log(`Trait ${traitIndex + 1} already known`);
            return this.CONTINUE;
        }
    }

    scoutRandomTrait() {
        const traitIndex = Math.min(d(8), d(8)) - 1;
        console.log(`Random Trait rolled is slot ${traitIndex + 1}`);
        this.checkConfig.successes -= 1;
        if (this.scoutedCharacter.coreTraits[traitIndex] == null) {
            this.fillCoreTraitSlot(this.scoutedCharacter, traitIndex);
            return this.NO_PROMPT;
        } else {
            console.log(`Trait ${traitIndex + 1} already known`);
            return this.CONTINUE;
        }
    }

    stateProgressExpeditions() {
        this.expeditionIndex += 1;
        if (this.expeditionIndex >= this.expeditions.length) {
            console.log("All expeditions have been updated");
            this.expeditionIndex = -1;
            this.replaceState(this.stateEmbark);
            return this.CONTINUE;
        }

        this.currentExpedition = this.expeditions[this.expeditionIndex];
        this.pushState(this.stateProgressExpedition)
        return this.NO_PROMPT;
    }

    stateProgressExpedition(input) {
        console.log(`Current Expedition: ${this.currentExpedition.name}`);
        if (this.currentExpedition.delay > 0) {
            this.currentExpedition.delay -= 1;
            console.log(`This expedition is delayed this month and for ${this.currentExpedition.delay} more months`)
        }
        this.replaceState(this.stateExpeditionConsumption);
        return this.CONTINUE;
    }

    getTrait(character, coreTraitType) {
        let emptyTraitIndex = [];
        for (let t = 0; t < character.coreTraits.length; ++t) {
            const trait = character.coreTraits[t];
            if (!trait) {
                emptyTraitIndex.push(t);
                continue;
            }
            const [traitType, value] = trait.split(' - ');
            if (traitType == coreTraitType) {
                return value;
            }
        }

        //trait not found - generate it
        const traitIndex = sample(emptyTraitIndex);
        const generatedTrait = sample(coreTraits[coreTraitType]);
        character.coreTraits[traitIndex] = `${coreTraitType} - ${generatedTrait}`;
        console.log(`Discovered: ${character.name} has trait ${coreTraitType} - ${generatedTrait}`);
        return generatedTrait;
    }

    stateExpeditionConsumption(input) {
        this.currentExpedition.members.forEach(character => {
            const diet = this.getTrait(character, "Diet") + " Diet";
            const lifestyle = this.getTrait(character, "Lifestyle") + " Lifestyle";

            const dietSupplies = Object.keys(this.currentExpedition.supplies).filter(s => s.includes("Diet") && (this.currentExpedition.supplies[s] || 0) > 0);
            const lifestyleSupplies = Object.keys(this.currentExpedition.supplies).filter(s => s.includes("Lifestyle") && (this.currentExpedition.supplies[s] || 0) > 0);
            if (dietSupplies.includes(diet)) {
                this.currentExpedition.supplies[diet] -= 1;
                console.log(`${character.name} eats 1 ${diet} Supply this month (${this.currentExpedition.supplies[diet]} left)`);
            } else if (dietSupplies.length > 0) {
                const replacementDiet = sample(dietSupplies);
                this.currentExpedition.supplies[replacementDiet] -= 1;
                character.morale -= 1;
                console.log(`${character.name} eats 1 ${replacementDiet} Supply this month, loosing 1 morale (${this.currentExpedition.supplies[replacementDiet] || 0} supplies left)`)
            } else {
                character.morale -= 2;
                console.log(`${character.name} eats only what little they can forage this month, loosing 2 morale`)
            }

            if (lifestyleSupplies.includes(lifestyle)) {
                this.currentExpedition.supplies[lifestyle] -= 1;
                console.log(`${character.name} uses 1 ${lifestyle} Supply this month (${this.currentExpedition.supplies[lifestyle]} left)`);
            } else if (lifestyleSupplies.length > 0) {
                const replacementLifestyle = sample(lifestyleSupplies);
                this.currentExpedition.supplies[replacementLifestyle] -= 1;
                character.morale -= 1;
                console.log(`${character.name} uses 1 ${replacementLifestyle} Supply this month, loosing 1 morale (${this.currentExpedition.supplies[replacementLifestyle] || 0} supplies left)`)
            } else {
                character.morale -= 2;
                console.log(`${character.name} sleeps on the ground this month, loosing 2 morale`)
            }

        })

        this.replaceState(this.stateExpeditionMovement);
        return this.CONTINUE;
    }

    stateExpeditionMovement(input) {
        const validOptions = this.getRouteOptionsFromLocatinoId(this.currentExpedition.location.id);
        const routePlanedLocatoinId = this.currentExpedition.route[0];

        const planValid = validOptions.includes(routePlanedLocatoinId);
        if (planValid) {
            this.currentExpedition.location = this.lookupLocation(routePlanedLocatoinId) || new Location(this.currentExpedition.location, parseInt(routePlanedLocatoinId.charAt(routePlanedLocatoinId.length - 1)))
            if (!this.locations.includes(this.currentExpedition.location)) {
                console.log(`${this.currentExpedition.location.toString()}`);
                console.log(`${this.currentExpedition.name} discovers ${this.currentExpedition.location.name}(${this.currentExpedition.location.id})`);
                this.locations.push(this.currentExpedition.location);
            } else {
                console.log(`${this.currentExpedition.name} moves into ${this.currentExpedition.location.name}(${this.currentExpedition.location.id})`);
            }
            this.currentExpedition.route.shift();
        } else {
            const parentLocation = this.currentExpedition.location.within;
            if (parentLocation) {
                this.currentExpedition.location = parentLocation;
                console.log(`${this.currentExpedition.name} is off route and beings heading back to base, reaching ${parentLocation.name}(${parentLocation.id})`);
            }
        }

        if (this.currentExpedition.location.id == this.locationRoot.id) {
            console.log(`${this.currentExpedition.name} has returned to base`);
            const index = this.expeditions.indexOf(this.currentExpedition);
            this.expeditions.splice(index, 1);
            this.expeditionIndex -= 1;

            Object.entries(this.currentExpedition.supplies).forEach(([supply, count]) => {
                this.supplies[supply] = (this.supplies[supply] || 0) + count;
                console.log(`${count} ${supply} Supplies added to the base supplies from ${this.currentExpedition.name}`);
            });

            this.popState();
        } else {
            this.replaceState(this.stateExpeditionGenerateEncounters);
        }

        return this.CONTINUE;
    }

    stateExpeditionGenerateEncounters(input) {
        this.encounters = [
            new Encounter(this.currentExpedition.location),
            new Encounter(this.currentExpedition.location)
        ]

        this.replaceState(this.stateExpeditionChooseEncounter);
        return this.NO_PROMPT;
    }


    stateExpeditionChooseEncounter(input) {
        let selection = parseInt(input);
        if (isNaN(selection) || selection <= 0 || selection > this.encounters.length) {
            this.encounters.forEach((e, index) => {
                console.log(`--------Encounter ${index + 1}--------`);
                console.log(e.longString());
            })

            console.log();
            this.encounters.forEach((e, index) => {
                console.log(`${index + 1}: ${e.toString()}`);
            });

            return "Which encounter will the expedition go on?";
        }

        this.currentEncounter = this.encounters[selection - 1];
        this.replaceState(this.stateExpeditionEncounterResolution);
        return this.NO_PROMPT;
    }

    stateExpeditionEncounterResolution(input) {
        const currentCheck = this.currentEncounter.event.check;
        if (currentCheck && !this.currentEncounter.checkLeader) {
            this.checkConfig = null;
            this.pushState(this.stateExpeditionChooseEncounterLeader);
            return this.NO_PROMPT;
        }

        if (this.checkConfig == null && currentCheck && this.currentEncounter.checkLeader) {
            this.checkConfig = {
                lead: this.currentEncounter.checkLeader,
                attribute: currentCheck.attribute,
                supply: currentCheck.type,
                pool: this.currentEncounter.checkLeader[currentCheck.attribute].getValue(),
                limit: this.currentEncounter.checkLeader[currentCheck.attribute].getValue()
            }
            this.pushState(this.stateCheck);
            return this.NO_PROMPT;
        }

        if (this.checkConfig && currentCheck && Object.keys(this.checkConfig).includes('successes')) {
            this.replaceState(this.stateExpeditionEncounterMorale);

            this.currentEncounter.passed = this.checkConfig.successes >= this.currentExpedition.location.difficulty;
            if (!this.currentEncounter.passed && currentCheck.fail) {
                if (currentCheck.fail.injuryCheckLeader) {
                    this.pushState(this.stateInjuryCheckLeader)
                }

                if (currentCheck.fail.injuryCheckAll) {
                    this.pushState(this.stateInjuryCheckAll)
                }

                if (currentCheck.fail.moraleAll) {
                    console.log(`All members of ${this.currentExpedition.name} morale ${currentCheck.fail.moraleAll}`);
                    this.currentExpedition.members.forEach(m => m.morale += currentCheck.fail.moraleAll);
                }

                if (currentCheck.fail.delay) {
                    console.log(`${this.currentExpedition.name} delayed for ${currentCheck.fail.delay} months`)
                    this.currentExpedition.delay += currentCheck.fail.delay;
                }
            }

            if (this.currentEncounter.passed && currentCheck.pass) {
                console.log("PLACEHOLDER PASS EVENT LOGIC");
            }

            if (this.currentEncounter.passed) {
                const reward = this.currentEncounter.reward;
                reward.supplies?.forEach(s => {
                    this.currentExpedition.supplies[s] = (this.currentExpedition.supplies[s] || 0) + this.currentExpedition.location.reward;
                    console.log(`${this.currentExpedition.name} gains ${this.currentExpedition.location.reward} ${s} Supplies (New Total: ${this.currentExpedition.supplies[s]})`);
                })

                reward.checkedSupplies?.forEach(s => {
                    const amount = this.inlineCheck({ limit: this.currentExpedition.location.reward, pool: this.currentExpedition.location.reward }).successes;
                    this.currentExpedition.supplies[s] = (this.currentExpedition.supplies[s] || 0) + amount;
                    console.log(`${this.currentExpedition.name} gains ${amount} ${s} Supplies (New Total: ${this.currentExpedition.supplies[s]})`);
                })

                if (reward.moraleAll) {
                    this.currentExpedition.members.forEach(m => {
                        m.morale += reward.moraleAll;
                        console.log(`${m.name} gains ${reward.moraleAll} morale (New Total: ${m.morale})`);
                    })
                }
            }

            return this.CONTINUE;
        }

        console.log("Encounter " + this.currentEncounter.toString() + " is not implemented yet");
        this.replaceState(this.stateExpeditionEncounterMorale);
        return this.CONTINUE;
    }

    stateInjuryCheckLeader(input) {
        console.log("INJURY CHECK PLACEHOLDER FOR LEADER " + this.currentEncounter.checkLeader.name);
        this.popState();
        return this.NO_PROMPT;
    }

    stateInjuryCheckAll(input) {
        console.log("INJURY CHECK PLACEHOLDER FOR ALL CHARACTERS ON " + this.currentExpedition.name);
        this.popState();
        return this.NO_PROMPT;
    }

    stateExpeditionChooseEncounterLeader(input) {
        const selectedCharacter = this.matchInputToCharacterList(input, this.currentExpedition.members);
        if (selectedCharacter) {
            this.currentEncounter.checkLeader = selectedCharacter;
            this.popState();
            return this.NO_PROMPT;
        }

        console.log(this.getCharacterListDisplay(this.currentExpedition.members, (m) => `B ${m.body.value || '?'} M ${m.mind.value || '?'} S ${m.soul.value || '?'}`));
        return "Please select a character to lead this event"
    }

    stateExpeditionEncounterMorale(input) {

        console.log("Expedition Encounter Morale Placeholder");
        this.popState();
        return this.CONTINUE;
    }

    stateEmbark(input) {
        if (input == 'n') {
            this.replaceState(this.stateUpdateRelationships);
            return this.NO_PROMPT;
        }

        if (input == 'y') {
            let exp = new Expedition();
            exp.location = this.locationRoot;
            this.expeditions.push(exp);
            this.planningExpedition = this.expeditions[this.expeditions.length - 1];
            this.pushState(this.stateEmbarkPlan);
            return this.NO_PROMPT;
        }

        return "Would you like to create another expidition this month? (y/n)";
    }

    stateEmbarkPlan(input) {
        const routeHead = this.planningExpedition.route[this.planningExpedition.route.length - 1] || this.locationRoot.id;
        let routeOptions = this.getRouteOptionsFromLocatinoId(routeHead);

        if (input == 'cancel') {
            this.expeditions.pop();
            this.popState();
            return this.NO_PROMPT;
        }

        if (input == 'unroute') {
            this.planningExpedition.route.pop();
            return this.NO_PROMPT;
        }

        if (input == 'embark') {
            if (this.planningExpedition.members.length < 1) {
                console.log("Expeditions require at least one member");
                return this.CONTINUE;
            }

            if (this.planningExpedition.route.length < 1) {
                console.log("Expeditions must have at least one destination routed");
                return this.CONTINUE;
            }

            this.popState();
            console.log("Expedition successfully embarks");
            return this.CONTINUE;
        }

        const tokens = input.split(' ');
        if (tokens.length > 1) {
            const command = tokens.shift();

            if (command == "pack" || command == "unpack") {
                let amount = parseInt(tokens.pop());
                if (!isNaN(amount)) {
                    const supplyType = tokens.join(' ');
                    if (command == "unpack") {
                        if ((this.planningExpedition.supplies[supplyType] || 0) < amount) {
                            console.log(`Not enough ${supplyType} to unpack`);
                            return this.CONTINUE;
                        }
                        amount *= -1;
                    }
                    if (command == "pack") {
                        if ((this.supplies[supplyType] || 0) < amount) {
                            console.log(`Not enough ${supplyType} to pack`);
                            return this.CONTINUE;
                        }
                    }
                    this.planningExpedition.supplies[supplyType] = (this.planningExpedition.supplies[supplyType] || 0) + amount;
                    this.supplies[supplyType] = (this.supplies[supplyType] || 0) - amount;
                }
            }
            if (command == 'route') {
                const ro = tokens.join(' ');
                if (!routeOptions.includes(ro)) {
                    console.log(`Invalid route option ${ro}`);
                    return this.CONTINUE;
                }

                this.planningExpedition.route.push(ro);
                return this.NO_PROMPT;
            }

            if (command == 'add' || command == 'remove') {
                const characterSelector = tokens.join(' ');
                const characters = this.getBaseCharacters().concat(this.planningExpedition.members);
                let character = this.matchInputToCharacterList(characterSelector, characters);
                if (!character) {
                    console.log(`Unknown Character ${characterSelector}`);
                    return this.CONTINUE;
                }
                if (command == 'add') {
                    if (this.planningExpedition.members.includes(character)) {
                        console.log(`Expedition already includes ${character.name}`);
                        return this.CONTINUE;
                    }
                    this.planningExpedition.members.push(character);
                }

                if (command == 'remove') {
                    if (!this.planningExpedition.members.includes(character)) {
                        console.log(`Expedition already does not include ${character.name}`);
                        return this.CONTINUE;
                    }

                    const index = this.planningExpedition.members.indexOf(character);
                    this.planningExpedition.members.splice(index, 1);
                }
            }
        }



        return `${this.expeditionString(this.planningExpedition)}

-Embark Planning Commands-
add/remove (character name)
route (location designation)
unroute
pack/unpack (Supply Type) (amount)
cancel
embark

-Available Routes-
${routeOptions.map(ro => this.routeStringDisplay(ro))}`
    }

    getRouteOptionsFromLocatinoId(locationId) {
        const location = this.lookupLocation(locationId);
        let routeOptions = [];
        if (location?.withith) {
            routeOptions.push(location.withith.id);
        }

        location?.contains.forEach((l, i) => {
            if (l) { routeOptions.push(l.id); }
            else { routeOptions.push(`${locationId}${i + 1}`); }
        });

        if (locationId != "R") {
            routeOptions.push(locationId.substring(0, locationId.length - 1));
        }

        return routeOptions;
    }

    lookupLocation(id) {
        return this.locations.find(l => l.id == id);
    }

    routeStringDisplay(ro) {
        for (let l = 0; l < this.locations.length; ++l) {
            const location = this.locations[l];
            if (location.id == ro) {
                return `${location.name} (${ro})`
            }
        }

        return `??? (${ro})`;
    }

    expeditionString(exp) {
        return `--${exp.name} (${exp.id})}--
Route: ${exp.route.map(ro => this.routeStringDisplay(ro))}
Members: ${exp.members.map(m => m.name)}
Supplies: ${JSON.stringify(exp.supplies)}
Location: ${this.routeStringDisplay(exp.location.id)}`
    }

    stateUpdateRelationships() {
        const baseCharacters = this.getBaseCharacters();
        let anyChanges = false;
        for (let i = 0; i < baseCharacters.length; ++i) for (let j = i + 1; j < baseCharacters.length; ++j) {
            if (i == j) continue;
            const characterA = baseCharacters[i];
            const characterB = baseCharacters[j];

            const aTrait = characterA.coreTraits[characterA.presentingTrait];
            const bTrait = characterB.coreTraits[characterB.presentingTrait];

            if (aTrait == null || bTrait == null) {
                continue;
            }

            if (aTrait == bTrait) {
                characterA.modifyTeamwork(characterB, 1);
                console.log(`${characterA.name} is bonding with ${characterB.name} over ${aTrait}`);
                anyChanges = true;
            } else {
                const aType = aTrait.split(' - ')[0];
                const bType = bTrait.split(' - ')[0];
                if (aType == bType) {
                    characterA.modifyTeamwork(characterB, -1);
                    console.log(`${characterA.name} is in a disagreement with ${characterB.name} over ${aType}`);
                    anyChanges = true;
                }
            }
        }

        if (!anyChanges) {
            console.log("No relationship updates this month");
        }

        this.replaceState(this.stateMonthBegin);
        return this.CONTINUE;
    }

    getTeamworkFor(character) {
        if (!character) return 0;

        const baseCharacters = this.getBaseCharacters();
        let groups = [baseCharacters];
        this.expeditions.forEach(e => groups.push(e.members));

        let result = 0;
        for (let g = 0; g < groups.length; ++g) {
            const group = groups[g];
            if (group.includes(character)) {
                group.forEach(member => result += character.teamwork[member.id] || 0);
            }
        }

        return result;
    }

    stateBase(input) {
        switch (input) {
            case "locations":
                return `Locations: ${this.locations.map(l => `${l.toString()}`).join('\n')}`;
            case "characters":
                return `Characters:\n ${this.characters.map(c => c.toString()).join('\n')}}`;
            case "expeditions":
                return `Expeditions: ${this.expeditions.map(e => e.toString()).join('\n')}`;
            case "supplies":
                return `Supplies: ${JSON.stringify(this.supplies)}`;
            default:
                return null;
        }
    }
}

module.exports = { Game };