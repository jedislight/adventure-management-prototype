const readline = require('readline');
const { Game } = require('./game');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
const game = new Game();
let input = "init";
let output = "";

function doNext() {
    output = game.getNext(input)
    rl.question(`${output}\n>`, (newInput) => {
        input = newInput;
        if (input == "exit") {
            rl.close();
            return;
        }

        doNext();
    });
}

doNext();