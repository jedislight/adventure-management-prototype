function d(num) {
    let faces = [];
    for (var i = 1; i <= num; ++i) {
        faces.push(i);
    }

    return shuffle(faces)[0];
}


function shuffle(array) {
    if (!array || !Array.isArray(array)) return;

    let currentIndex = array.length, randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex != 0) {

        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }

    return array;
}

function sample(array) {
    if (!Array.isArray(array) && array) array = Object.entries(array)
    const shuffled = shuffle(array);
    return shuffled[0];
}

module.exports = { d, shuffle, sample }