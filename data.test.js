const { Encounter, Location } = require('./data');


test('Encounter generation tests', () => {
    const root = new Location();
    const loc = new Location(root, 1);
    const loc2 = new Location(loc, 1);

    const exp = new Encounter(loc2);
    console.log(exp.toString());
    console.log(exp.longString());
});
